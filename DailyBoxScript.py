import serial
import time
from ccTools.ccTalk import *

ser=serial.Serial('/dev/ttyUSB0', 9600, timeout=1)

def sendMessage(header, data='', source=1, destination=2):
    request = ccTalkMessage(header=header, payload=data, source=source, destination=destination)
    #print " -> " + str(request)
    ser.write(request.raw())
    data = ser.read(50)
    messages = parseMessages(data)[1]
    #print " <- " + str(messages[1])
    return messages[1]

print "[*] Pinging device..."
ok = False

#Wait for device to be initiated
while ok!=True:
    try:
        response = sendMessage(0xfe)
    except:
        continue
    if response.payload.header==0:
        ok = True
    else:
        print response.payload.header

print "[*] Request manufacturer ID..."
sendMessage(246)

print "[*] Request product code..."
sendMessage(244)

# print "[*] Request master inhibit status..."
# sendMessage(227)

# print "[*] Request master inhibit status..."
# sendMessage(228, '\x01')

# print "[*] Request inhibit status..."
# sendMessage(230)

print "[*] Set inhibit status..."
sendMessage(231,'\xff\x00')

print "[*] Request master inhibit status..."
sendMessage(228, '\x01')

# print "[*] Request comms revision..."
# sendMessage(4)

# print "[*] Request default sorter path..."
# sendMessage(188)

# print "[*] Request coin IDs..."
# for i in range(1,16):
#     sendMessage(184,chr(i))

print "[*] Entering polling loop..."
event = 0
while True:
    try:
        time.sleep(0.05)
        message = sendMessage(header=229)
        if message.payload.header==0:
            data = message.payload.data
            if ord(data[0])>event:
                event = ord(data[0])
                if ord(data[1])==0:
                    print " [!] Error"
                elif ord(data[1])==6:
                    print " [-] PLN 5.00-"
                elif ord(data[1])==5:
                    print " [-] PLN 2.00-"
                elif ord(data[1])==4:
                    print " [-] PLN 1.00-"
                elif ord(data[1])==3:
                    print " [-] PLN 0.50-"
                elif ord(data[1])==2:
                    print " [-] PLN 0.20-"
                elif ord(data[1])==1:
                    print " [-] PLN 0.10-"
    except KeyboardInterrupt, e:
        print "Quitting..."
        break

ser.close()

